<!DOCTYPE html>
<?php
require "connectivity.php";
$query1 = "SELECT class.class_description FROM class";
$get = mysqli_query( $con, $query1 );
$option = '';
while ( $row = mysqli_fetch_assoc( $get ) ) {
	$option .= '<option value = "' . $row[ 'class_description' ] . '">' . $row[ 'class_description' ] . '</option>';
}
?>


<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Globo Gym</title>

	<!-- Bootstrap CSS and bootstrap datepicker CSS used for styling the demo pages-->
	<link rel="stylesheet" href="css/datepicker.css">
	<link rel="stylesheet" href="css/bootstrapdate.css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
	 <script src="jquery/jquery.js"></script>
	 <script src="js/login.js"></script>


	<!--DatePicker & Table -->
	<script src="//code.jquery.com/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="jquery/jquery.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

	<style>

		.modalLink {
			color: black;
			text-decoration: underline;
		}
		#info { color: #fed136; 
			font-weight: bold; 
			text-align: left;}
		
		#info1 { text-align: left;}


		.table-content {
			border-top: #CCCCCC 4px solid;
			width: 50%;
		}
		
		.table-content th {
			padding: 5px 20px;
			background: #F0F0F0;
			vertical-align: top;
		}
		
		.table-content td {
			padding: 5px 20px;
			border-bottom: #F0F0F0 1px solid;
			vertical-align: top;
		}
		
		.img-center {margin:0 auto;}
	</style>



	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/timetable.css" rel="stylesheet">
	<link href="css/modallogin.css" rel="stylesheet">
	<link href="css/table.css" rel="stylesheet">


	<!-- Custom Fonts -->
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href='//fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

	<!-- Theme CSS -->
	<link href="css/agency.min.css" rel="stylesheet">
</head>

<body id="page-top" class="index">

	<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header page-scroll">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
			



				<a class="navbar-brand page-scroll" href="#page-top">Globo Gym</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="hidden">
						<a href="#page-top"></a>
					</li>

					<li>
						<a class="page-scroll" href="#facilities">Facilities</a>
					</li>
					<li>
						<a class="page-scroll" href="#portfolio">Classes</a>
						<li>
							<a class="page-scroll" href="#member">Membership</a>
						</li>
						<li>
							<a class="page-scroll" href="#timetableinfo">Timetable</a>
						</li>
						<li>
							<a class="page-scroll" href="#contact">Contact Us</a>
						</li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>


	<!-- Header -->
	<header>
		<div class="container">
			<div class="intro-text">
				<div class="intro-lead-in">Welcome To Globo Gym!</div>
				<div class="intro-heading">Fitter Faster Further</div>
				<a href="#facilities" class="page-scroll btn btn-xl">Tell Me More</a>
			</div>
		</div>
	</header>

	<!-- Facilities Section -->
	<section id="facilities">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">Facilities</h2>
					<h3 class="section-subheading text-muted">Globo Gym offers a variety of modern facilities to meet your needs, from the latest equipment to a 25m swimming pool, you will be sure to find something you enjoy! </h3>
				</div>
			</div>
			<div class="row text-justify">
				<div class="col-md-3">
					<img src="images/fitnessclass.jpg" class="img-responsive img-center" style="width: 150px; height: 120px;">
					</i>
					</span>
					<h4 class="service-heading" align="center">Fitness Class</h4>
					<p class="text-muted">Whether you want to lose weight, tone up, gain muscle or improve strength and endurance levels, we have a wide variety of sessions to achieve your goals in a friendly environment and with instructors to give your motivation a helping hand.</p>
				</div>
				<div class="col-md-3">
					<img src="images/personaltraining.jpg" class="img-responsive img-center" style="width: 150px; height: 120px;">
					</i>
					<h4 class="service-heading" align="center">Personal Training</h4>
					<p class="text-muted">Our personal trainers are qualified level 3 fitness professionals who specialise in many different areas such as weight management, rehabilitation, sports specifics and nutrition. Your trainer will ensure that you get the very best from each workout.</p>
				</div>
				<div class="col-md-3">
					<img src="images/gym_image.jpg" class="img-responsive img-center" style="width: 150px; height: 120px;">
					</i>
					<h4 class="service-heading" align="center">Gym</h4>
					<p class="text-muted">A fully air conditioned fitness suite offers the latest cardio, resistance and free weights equipment. So, whether your goal is to lose weight, look good or feel great our team of fully qualified fitness instructors are on hand to help you achieve your goal.</p>
				</div>
				<div class="col-md-3">
					<img src="images/pool.jpg" class="img-responsive img-center" style="width: 150px; height: 120px;">
					</i>
					<h4 class="service-heading" align="center">Swimming Pool</h4>
					<p class="text-muted">Swimming is a great form of exercise for the whole body. The swimming pool provides a great environment for warming up or cooling down after a strenuous gym workout but is also a very effective exercise in its own right. </p>
				</div>
			</div>
		</div>
	</section>

	<!-- Portfolio Grid Section -->
	<section id="portfolio" class="bg-light-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">Our Classes</h2>
					<h3 class="section-subheading text-muted">A range of classes are available and taught by trainers. Select a class to view more information on the class and view our timetable to check when they take place! Don't forget to book in advance to ensure a spot! </h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6 portfolio-item">
					<a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
						<div class="portfolio-hover">
							<div class="portfolio-hover-content">
								<i class="fa fa-plus fa-3x"></i>
							</div>
						</div>
						<img src="images/spinclass.jpg" class="img-responsive" alt="">
					</a>
					<div class="portfolio-caption">
						<h4>Spinning</h4>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 portfolio-item">
					<a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
						<div class="portfolio-hover">
							<div class="portfolio-hover-content">
								<i class="fa fa-plus fa-3x"></i>
							</div>
						</div>
						<img src="images/kettlebellclass.jpg" class="img-responsive" alt="">
					</a>
					<div class="portfolio-caption">
						<h4>KettleBells</h4>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 portfolio-item">
					<a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
						<div class="portfolio-hover">
							<div class="portfolio-hover-content">
								<i class="fa fa-plus fa-3x"></i>
							</div>
						</div>
						<img src="images/trxclass.jpg" class="img-responsive" alt="">
					</a>
					<div class="portfolio-caption">
						<h4>TRX</h4>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 portfolio-item">
					<a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
						<div class="portfolio-hover">
							<div class="portfolio-hover-content">
								<i class="fa fa-plus fa-3x"></i>
							</div>
						</div>
						<img src="images/bodypumpclass.jpg" class="img-responsive" alt="">
					</a>
					<div class="portfolio-caption">
						<h4>BodyPump</h4>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 portfolio-item">
					<a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
						<div class="portfolio-hover">
							<div class="portfolio-hover-content">
								<i class="fa fa-plus fa-3x"></i>
							</div>
						</div>
						<img src="images/pilatesclass.jpg" class="img-responsive" alt="">
					</a>
					<div class="portfolio-caption">
						<h4>Pilates</h4>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 portfolio-item">
					<a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
						<div class="portfolio-hover">
							<div class="portfolio-hover-content">
								<i class="fa fa-plus fa-3x"></i>
							</div>
						</div>
						<img src="images/circuitsclass.jpg" class="img-responsive" alt="">
					</a>
					<div class="portfolio-caption">
						<h4>Circuits</h4>
					</div>
				</div>
								<div class="col-md-4 col-sm-6 portfolio-item">
					<a href="#portfolioModal7" class="portfolio-link" data-toggle="modal">
						<div class="portfolio-hover">
							<div class="portfolio-hover-content">
								<i class="fa fa-plus fa-3x"></i>
							</div>
						</div>
						<img src="images/aquaaerobics.jpg" class="img-responsive" alt="">
					</a>
					<div class="portfolio-caption">
						<h4>Aqua Aerobics</h4>
					</div>
				</div>
								<div class="col-md-4 col-sm-6 portfolio-item">
					<a href="#portfolioModal8" class="portfolio-link" data-toggle="modal">
						<div class="portfolio-hover">
							<div class="portfolio-hover-content">
								<i class="fa fa-plus fa-3x"></i>
							</div>
						</div>
						<img src="images/adultswim.jpg" class="img-responsive" alt="">
					</a>
					<div class="portfolio-caption">
						<h4>Adult Swim Lesson</h4>
					</div>
				</div>
								<div class="col-md-4 col-sm-6 portfolio-item">
					<a href="#portfolioModal9" class="portfolio-link" data-toggle="modal">
						<div class="portfolio-hover">
							<div class="portfolio-hover-content">
								<i class="fa fa-plus fa-3x"></i>
							</div>
						</div>
						<img src="images/childswim.jpg" class="img-responsive" alt="">
					</a>
					<div class="portfolio-caption">
						<h4>Child Swim Lesson</h4>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- About Section -->
	<section id="member">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">Membership</h2>
					<h3 class="section-subheading text-muted">Globo Gym truly has something to offer everyone in a modern, family environment. Our affordable memberships are packed with many benefits.</h3>
				</div>
				<div class="col-md-3 text-center">
					<img src="images/header_image_5.jpg" class="img-circle" style="width: 180px; height: 150px;">
					</i>
					<h4 class="service-heading">Full Fitness</h4>
					<h5 class="service-heading">Unlimited Classes and full access to gym</h5>
					<p class="text-muted">Our full fitness membership allows you to access unlimited classes and use the gym at any time of the day! Both children and adults are welcome to sign up for this membership. This is a non contract membership with a direct debit payment each month.</p>
				</div>
				<div class="col-md-3 text-center">
					<img src="images/gym.jpg" class="img-circle" style="width: 180px; height: 150px;">
					</i>
					<h4 class="service-heading">Gym Only</h4>
					<h5 class="service-heading">Full access to gym</h5>
					<p class="text-muted">Our gym only membership is geared towords those who are not interested in classes, or will not attend classes very often. Both children and adults are wecome to sign up for this membership. This is a non contract membership with a direct debit payment each month.  </p>
				</div>
				<div class="col-md-3 text-center">
					<img src="images/kettlebellclass.jpg" class="img-circle" style="width: 180px; height: 150px;">
					</i>
					<h4 class="service-heading">Class Only</h4>
					<h5 class="service-heading">Unlimited access to classes</h5>
					<p class="text-muted">Our class only membership is geared towords those who are attending classes very regularly. Unfortunatly only adults are wecome to sign up for this membership. This is a non contract membership with a direct debit payment each month. </p>
				</div>
				<div class="col-md-3 text-center">
					<img src="images/fitnessclass_main.jpg" class="img-circle" alt="" style="width: 180px; height: 150px;">
					</i>
					<h4 class="service-heading">Family Only</h4>
					<h5 class="service-heading">2 Adults and 2 Children unlimited access</h5>
					<p class="text-muted">Our family membership is ideal for those who have a kids who also attend the gym. This membership is valid for 2 adults (+18) and 2 children. Adults can access all classes and gym and children can access the gym at any time. This is a non contract membership with a direct debit payment each month. </p>
				</div>
			</div>


		</div>
	</section>


	<!-- Timetable Section -->
	<section id="timetableinfo" class="bg-light-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 class="section-heading">Timetable</h2>
					<h3 class="section-subheading text-muted">View our timetable and book classes! To book a class online you need your member ID number! If you are not a member and wish to book a class please contact us by phone or call into our center and we will be happy to assist you!</h3>
		<!-- Date Range Filter -->
				<div id = "daterangefilter">
				<div class="col-md-2">
					<input type="text" name="From" id="From" class="form-control" placeholder="From Date"/>
				</div>
				<div class="col-md-2">
					<input type="text" name="to" id="to" class="form-control" placeholder="To Date"/>
				</div>
				<div class="col-md-2">
					<input type="button" name="range" id="range" value="Range" class="btn btn-success"/>
				</div>
				<div class="clearfix"></div>
					<br/></div>
				
            	<div id = "timetable">
	            <div style="overflow:scroll;height:200px;width:100%;overflow:auto">
				<table class="table table-bordered">
				<thead style: bgcolor="#FED136";>
				<tr style="color: white; ">
				    <th><strong>CLASS</strong>
					</th>
				    <th><strong>DATE</strong>
					</th>
					<th><strong>TIME</strong>
					</th>
					<th><strong>LOCATION</strong>
					</th>
					<th><strong>BOOK NOW!</strong>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$count = 1;
				$sel_query = "SELECT schedule.*, class.class_description, area.area_description FROM globo_gym1.schedule , globo_gym1.class, globo_gym1.area WHERE schedule.class_ID = class.class_ID AND class.area_ID = area.area_ID";
				$result = mysqli_query( $con, $sel_query );
				while ( $row = mysqli_fetch_assoc( $result ) ) {
					?>
				
				<tr>
				
					<td align="left">
						<?php echo $row["class_description"]; ?>
					</td>
					<td align="left">
						<?php echo $row["date"]; ?>
					</td>
					<td align="left">
						<?php echo $row["startTime"]; ?>
					</td>
					<td align="left">
						<?php echo $row["area_description"]; ?>
					</td>
					<td align="center">
					<a class="modalLink" href="#myModal" data-toggle="modal" data-target="#myModal" id="<?php echo $row["id"]; ?>" <button>
               			Book
						</button></a>
					</td>
					</td>
				</tr>
				<?php $count++; } ?>
			</tbody>
			</div>
		</table>
	</div>

			
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
</div>
</div>
			
		</div>
		</div>

		</div>

	</section>


	<!-- Contact Section -->
	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">Contact Us</h2>
					<h3 class="section-subheading text-muted">If you have any questions or queries please do not hesitate to contact us by phone or using the contact form below.</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<form name="sentMessage" id="contactForm" novalidate>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
									<p class="help-block text-danger"></p>
								</div>
								<div class="form-group">
									<input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
									<p class="help-block text-danger"></p>
								</div>
								<div class="form-group">
									<input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
									<p class="help-block text-danger"></p>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-lg-12 text-center">
								<div id="success"></div>
								<button type="submit" class="btn btn-xl">Send Message</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<!-- FOOTER -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<span class="copyright">Copyright &copy; Globo Gym 2017</span>
				</div>

			</div>
		</div>
	</footer>

	<!-- Portfolio Modals -->

	<!-- Portfolio Modal 1 -->
	<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="modal-body">
								
								<h2>Spinning</h2>
								<p class="item-intro text-muted">About Spinning</p>
								<p align="left">Spinning is the popular instructor led group fitness activity sweeping the nation! Participants ride a number of stationary bicycles in a fitness studio alongside excellent instructors and a great soundtrack. We have created a programme of spinning classes that has something for everyone; whether you are a beginner searching for something new or if you’re looking for a tough endurance workout. Spinning is a great way to get in shape and stay in shape!
								<p class="item-intro text-muted">The Benefits of Spinning</p>
								<p align="left">>Spinning is an excellent cardio based exercise allowing you to increase muscle strength, fitness, and endurance! Not only will our spin classes give you a fabulous workout suited to your ability, it will also allow you the opportunity to meet fellow spinners. You should try our classes at least once, and we are confident you’ll keep coming back!</p>
								<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Portfolio Modal 2-->
	<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="modal-body">
								<h2>Kettlebells</h2>
								<p class="item-intro text-muted">About Kettlebells</p>
								<p align="left">A fun class combining cardiovascular, strength and flexibility training. Kettlebell training is a great full body workout, targeting muscles through a wider range of motion and engaging the whole body. Expect to do basic movements such as swinging, snatch and the clean and jerk. Expect the class to last 45 minutes. It’s a great class for anyone looking for a lean physique.</p>
								<p class="item-intro text-muted">The Benefits of Spinning</p>
								<p align="left">Our Kettlebell training class focuses on working your chest, shoulder, legs and your grip. The unique handle and awkward weight distribution will work your core harder than dumbbells ever will while burning more calories. Kettlebells are fantastic for working all your body’s major muscles while burning body fat and building overall body strength. Regular Kettlebell and Cardio training exercises can help you achieve the body you’ve always wanted.</p> 
								
								<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Portfolio Modal 3 -->
	<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="modal-body">
								
								<h2>TRX</h2>
								<p class="item-intro text-muted">What is TRX? </p>
								<p align="left">Total Body Resistance (TRX) is the brainchild of Navy Seal Randy Hetrick. Using a series of straps attached to an A-frame or wall, gravity works with your body weight to produce resistance. To modify an exercise, simply move away to increase the load and closer to reduce it. </p>
								<p align="left">Whatever you do, your core does it too and in a single exercise you use way more muscles, so results sky rocket. Target train or total body workout, your strength, balance and flexibility will thank you.
								Because TRX is based around a series of straps attached to an A-frame or wall, the opportunities are endless. There’s a whole host of exercises and activities you can do with TRX, making it an essential piece of kit for anyone who’s looking to work on their core, improve cardio fitness or build strength. </p>
								<p align="left">TRX is for any fitness level: it uses your own body weight to provide the resistance, and a shift of body angle or stance can lessen or increase that resistance. So it is great for beginners and easy to progress as you get stronger.</p>
							
								<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Portfolio Modal 4 -->
	<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="modal-body">
								
								<h2>Body Pump</h2>
								<p class="item-intro text-muted">What is bodypump? </p>
								
								<p align="left">LES MILLS BODYPUMP™ is ideal for anyone looking to get lean, toned and fit as fast as possible.
								Performing repeated exercises using light to moderate weights, Body Pump will give you a full body workout and burn up to 590 calories*. Instructors will coach you through the scientifically proven moves and techniques pumping out encouragement, motivation and great music – helping you achieve much more than you can on your own! You’ll leave the class feeling challenged and motivated, ready to come back for more. Expect the class to last up to an hour and burn lots of calories whilst enjoying your workout.</p>
								<p class="item-intro text-muted">The benifits of body pump </p>
								<p align="left">Body Pump focuses on all your major muscles, building body strength and helping you get lean and toned. Burning calories and improving general fitness all help to keep you healthy, lowering the risks of heart disease and diabetes as well as increasing your overall energy and well-being.</p>
								<p align="left">Get lean. Tone muscle. Get fit.</p>
								<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Portfolio Modal 5 -->
	<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="modal-body">
								
								<h2>Pilaties</h2>
								<p class="item-intro text-muted">What is pilaties?</p>
								<p align="left">With the intention of strengthening your entire body, Pilates places particular emphasis on improving general wellbeing and fitness through core strength. Pilates classes at 24/7 Fitness are performed in one of our large fitness studios. Practitioners link regular Pilate’s classes with improvements to muscle tone, balance, posture and mobility as well as the classes’ ability to reduce stress and tension levels. With something to offer for all ages and ability levels, Pilates is popular among elite athletes and fitness beginners alike. Pilates is fantastic in combination with other training techniques and can benefit full body strength and flexibility as well as reducing the risk of injury.</p>
								<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Portfolio Modal 6 -->
	<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="modal-body">
								
								<h2>Circuits</h2>
								<p class="item-intro text-muted">What is Circuits?</p>
								<p align="left">Running, squatting, lunging, pumping, skipping, hopping, pressing, spinning; you name it circuits has it! Circuit training is a form of body conditioning or resistance training using high-intensity aerobics. It targets strength building and muscular endurance. An exercise "circuit" is one completion of all prescribed exercises in the program. When one circuit is complete, you will start the first exercise again for the next circuit. The time between exercises in circuit training is short, often with rapid movement to the next exercise.</p>
								<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	
	<!--Portfolio Modal 7 -->
	<div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="modal-body">
								
								<h2>Aqua Aerobics</h2>
								<p class="item-intro text-muted">What is Aqua Aerobics?</p>
								<p align="left">Aqua Aerobics classes are a great way to mix up a traditional gym workout with the wondrous benefits of water. It only takes a few aqua aerobics sessions and you’ll see a big difference in your overall fitness.
								Also know as water aerobics or sometimes waterobics, typical classes will involve all the exercises you would expect in a ‘dry’ class. But there are added bonuses when you exercise in water.</p>
								<p class="item-intro text-muted">What are the benifits?</p>
								<p align="left">1. Water supports the body, putting less stress on your joints and muscles <br> 
								   2. Working out in water helps build strength. Fighting against the push of the water activates your muscles <br> 
								   3. Water pressure helps put less strain on the heart by moving blood around the body <br>
								   4. The impact of gravity is less in the water allowing a greater range of motion<br>
								   5. Working out in water helps prevent overheating, helping you exercise for longer<br>
								   6. And… it’s fun! It is not often you can say that about a workout.</p>
								<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	
	<!--Portfolio Modal 8-->
	<div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-12" "col-sm-6">
							<div class="modal-body">
								
								<h2>Adult Swimming Lessons</h2>
								<p class="item-intro text-muted">Methods and Mechanics</p>
								<p>Whether you're a complete beginner or an expert swimmer, we have programmes to help you achieve what you want in the water.</p>
								
								<div class = "col-md-4">
								<p class="item-intro text-muted" style="color: #fed136; font-weight: bold;">Complete Beginners </p>
									<p align="left">Perfect for total non-swimmers who find it difficult to swim a length or put their face in the water.</p> </div>
								<div class = "col-md-4">
								<p class="item-intro text-muted" style="color: #fed136; font-weight: bold;">Front Crawl</p>
									<p align="left">For those who can already swim and glide with their face in the water, and would like to swim a smooth, fluid front crawl.</p></div>
								<div class = "col-md-4">
								<p class="item-intro text-muted" style="color: #fed136; font-weight: bold;">Butterfly</p>
									<p align="left">A stroke for confident swimmers and excellent for lower back mobility due to its continuous wave like undulating movement.</p></div>
								
								<div class = "col-md-4">
								<p class="item-intro text-muted" style="color: #fed136; font-weight: bold;">Backstroke</p>
									<p align="left">For those who can already swim and glide with their face in the water, and would like to swim a smooth, fluid backstroke.</p></div>
								<div class = "col-md-4">
								<p class="item-intro text-muted" style="color: #fed136; font-weight: bold;">Breaststroke</p>
									<p align="left">A therapeutic stroke ideal for calming the nervous system, reducing anxiety alongside building flexibility. </p> </div>
								<div class = "col-md-4">
								<p class="item-intro text-muted" style="color: #fed136; font-weight: bold;">Advanced Technique</p>
									<p align="left">For more advanced swimmers who seek expert professional guidance on refining stroke techniques, improving speed, building strength and boosting stamina, as well as covering starts, turns and dives.</p></div>
								
								<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!--Portfolio Modal 9 -->
	<div class="portfolio-modal modal fade" id="portfolioModal9" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="modal-body">
								
								<h2>Childrens Swimming Lessons</h2>
								<p class="item-intro text-muted">What levels are there?</p>
								<div id = "info">Ducklings: </div> 
								<div id = "info1">This is our beginner level where children will learn water confidence, how to float, submerge their face and to jump in.</br>
								<div id = "info">Tadpoles: </div>
								This is a step up from beginners where children will learn how to push and glide on their front and back, retrieve objects from below the surface and breaststroke arms.</br>
								<div id = "info">Frogs: </div>
								At this level children leave front crawl arm action unaided, backcrawl and breast stroke leg action.</br>
								Penguins: Now confident and independent in shallow water they can improve technique and advance their skills.</br>
								<div id = "info">Seals: </div>
								Further development with technique and learning more skills.</br>
								<div id = "info">Sharks: </div>
								Confident in deep water and beginning to build stamina and strength along with technique. Continuous development of skills and strokes.</br>
								<div id = "info">Stingray: </div>
								More stamina, strength and skills.</p></div>
								<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 



	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>

	<!-- Ajax Date Range Filter -->
	<script>
		$( document ).ready( function () {
			$.datepicker.setDefaults( {
				dateFormat: 'yy-mm-dd'
			} );
			$( function () {
				$( "#From" ).datepicker();
				$( "#to" ).datepicker();
			} );
			$( '#range' ).click( function () {
				var From = $( '#From' ).val();
				var to = $( '#to' ).val();
				if ( From != '' && to != '' ) {
					$.ajax( {
						url: "filter.php",
						method: "POST",
						data: {
							From: From,
							to: to
						},
						success: function ( data ) {
							$( '#timetable' ).html( data );
						}
					} );
				} else {
					alert( "Please Select the Date" );
				}
			} );
		} );
	</script>
	
	<script>
		$(function(){
			$("#loading").hide();
			var message_status = $("#status");
			$("td[contenteditable=true]").blur(function(){
        	var field_userid = $(this).attr("id") ;
			var value = $(this).text() ;
				$.post('update.php' , field_userid + "=" + value, function(data){

					if(data != '')
            {
                message_status.show();
                message_status.text(data);
                //hide the message
                setTimeout(function(){message_status.hide()},1000);
            }
        });
    });
});
</script>

    <script>
$('.modalLink').click(function(){
    var id=$(this).attr('id');
    $.ajax({url:"edit.php?id="+id,cache:false,success:function(result){
        $(".modal-content").html(result);
    }});
});
</script>
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	

	<!-- Contact Form JavaScript -->
	<script src="js/jqBootstrapValidation.js"></script>
	<script src="js/contact_me.js"></script>

	<!-- Theme JavaScript -->
	<script src="js/agency.min.js"></script>

</body>

</html>